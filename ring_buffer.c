#include "ring_buffer.h"


/* Data gets added to rear end of the queue */
void enqueue(int data) {
	if ((front == 0 && rear == BUFFER_SIZE - 1) || rear == front - 1) {
		printf("Buffer is full. Overflow detected\n");
	} else {
		if ((front == -1 && rear == -1)) {
			front = rear = 0;
		} else if (rear == BUFFER_SIZE - 1) {
			rear = 0;
		} else {
			rear++;
		}
		printf("Added %d to buffer\n", data);
		cqueue[rear] = data;
	}
}


/* Data gets removed from front end of the queue */
int dequeue() {
	if (front == -1 && rear == -1) {
		printf("Buffer is empty. Underflow detected\n");
	} else {
		int data = cqueue[front];
		printf("Removed %d from buffer\n", data);
		if (front == BUFFER_SIZE - 1) {
			front = 0;
		} else if (front == rear) {
			front = rear = -1;
		} else {
			front++;
		}
		return data;
	}
}


void print_buffer() {
	if (front == -1 && rear == -1) {
		printf("Buffer is empty\n");
	} else {
		printf("Buffer contents\n");
	        if (front < rear) {
			for (int i = front; i <=rear; i++)
                		printf("[%d]: %d -> ", i, cqueue[i]);
	        } else {
			for (int j = 0; j <= rear; j++)
        	        	printf("[%d]: %d -> ", j, cqueue[j]);
			for (int i = front; i < BUFFER_SIZE; i++)
                		printf("[%d]: %d -> ", i, cqueue[i]);
        	}
		printf("\n");
	}
}

int main() {
	//Perform queue operations here
}
